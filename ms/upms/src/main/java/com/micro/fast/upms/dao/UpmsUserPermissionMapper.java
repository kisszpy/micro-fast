package com.micro.fast.upms.dao;

import com.micro.fast.upms.pojo.UpmsUserPermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UpmsUserPermissionMapper {
    int deleteByPrimaryKey(Integer userPermissionId);

    int insert(UpmsUserPermission record);

    int insertSelective(UpmsUserPermission record);

    UpmsUserPermission selectByPrimaryKey(Integer userPermissionId);

    int updateByPrimaryKeySelective(UpmsUserPermission record);

    int updateByPrimaryKey(UpmsUserPermission record);

}