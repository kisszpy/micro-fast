package com.micro.fast.upms.dao;

import com.micro.fast.common.dao.SsmMapper;
import com.micro.fast.upms.pojo.UpmsRole;
import feign.Param;

import javax.management.relation.Role;
import java.util.List;

public interface UpmsRoleMapper extends SsmMapper<UpmsRole,Integer> {
    int deleteByPrimaryKey(Integer id);

    @Override
    int insert(UpmsRole record);

    @Override
    int insertSelective(UpmsRole record);

    @Override
    UpmsRole selectByPrimaryKey(Integer id);

    @Override
    int updateByPrimaryKeySelective(UpmsRole record);

    @Override
    int updateByPrimaryKey(UpmsRole record);

    @Override
    List<UpmsRole> selectByCondition(UpmsRole record);

    @Override
    int deleteByPrimaryKeys(List<Integer> integers);

    List<Role> selectRoleJoinWithUserId(@Param("userId") Integer userId);
}