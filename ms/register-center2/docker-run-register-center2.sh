#!/usr/bin/env bash
docker run -d   -p 10004:10004 -p 10005:10005 registry.cn-hangzhou.aliyuncs.com/lishouyu/hub:ms-register-center2V0.0.1-SNAPSHOT  \
 java -jar \
 -Dspring.cloud.config.uri=http://config:server@172.17.0.1:10000/ \
 -Deureka.client.serviceUrl.defaultZone=http://micro:fast@172.17.0.1:10002/eureka \
 -Deureka.instance.prefer-ip-address=true \
 -Deureka.instance.ip-address=172.17.0.1 \
 -Dspring.cloud.config.profile=deploy \
 /workhome/app.jar